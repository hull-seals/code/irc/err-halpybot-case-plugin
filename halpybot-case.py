from os import path

from errbot import BotPlugin, webhook, botcmd


class halpybot_case(BotPlugin):
    """
    HalpyBOT case plugin for the hullseals
    """


    @webhook
    def newcase(self, payload):
        self.log.debug("The incoming casepayload was :" + str(payload))

        #decode platform
        if payload.get('platform') == "1":
            caseplatform = "PC"
        elif payload.get('platform') == "2":
            caseplatform = "XB1"
        elif payload.get('platform') == "3":
            caseplatform = "PS4"
        else:
            caseplatform = "UNKNOWN"

        #is this a code black
        if payload.get('canopy_breached') == "1":
            #decode synth
            if payload.get('can_synth') == "1":
                cansynth = "No"
            else:
                cansynth = "Yes"

            #build message
            mymessage = (
                f"xxxx CBCASE xxxx\n"
                f" CMDR: {payload['cmdr_name']} -- Platform: {caseplatform} \n"
                f" System: {payload['system']} -- Hull: {payload['hull']} \n"
                f" Can synth: {cansynth} -- O2 timer: {payload['o2_timer']} \n"
                f"xxxx NEWCASE xxxx\n"
            )
            #build short message
            myshortmessage = (
                f" New CODE BLACK Case -- <@&744998165714829334\> \n"
                f" O2: {payload['o2_timer']} Synth: {cansynth} CMDR: {payload['cmdr_name']} Sys: {payload['system']} H: {payload['hull']} P: {caseplatform}\n"
            )
        elif payload.get('platform') == "1":
            #build message
            mymessage = (
                f"xxxx PCCASE xxxx\n"
                f" CMDR: {payload['cmdr_name']} -- Platform: {caseplatform} \n"
                f" System: {payload['system']} -- Hull: {payload['hull']} \n"
                f"xxxx NEWCASE xxxx\n"
            )
            #build short message
            myshortmessage = (
                f"New PC Case Available -- <@&744998165714829334\> \n"
                f" CMDR: {payload['cmdr_name']} Sys: {payload['system']} H: {payload['hull']} P: {caseplatform}\n"
            )

        elif payload.get('platform') == "2":
            #build message
            mymessage = (
                f"xxxx XBCASE xxxx\n"
                f" CMDR: {payload['cmdr_name']} -- Platform: {caseplatform} \n"
                f" System: {payload['system']} -- Hull: {payload['hull']} \n"
                f"xxxx NEWCASE xxxx\n"
            )
            #build short message
            myshortmessage = (
                f"New XB1 Case Available -- <@&744998165714829334\> \n"
                f" CMDR: {payload['cmdr_name']} Sys: {payload['system']} H: {payload['hull']} P: {caseplatform}\n"
            )

        elif payload.get('platform') == "3":
            #build message
            mymessage = (
                f"xxxx PSCASE xxxx\n"
                f" CMDR: {payload['cmdr_name']} -- Platform: {caseplatform} \n"
                f" System: {payload['system']} -- Hull: {payload['hull']} \n"
                f"xxxx NEWCASE xxxx\n"
            )
            #build short message
            myshortmessage = (
                f"New PS4 Case Available -- <@&744998165714829334\> \n"
                f" CMDR: {payload['cmdr_name']} Sys: {payload['system']} H: {payload['hull']} P: {caseplatform}\n"
            )

        else:
            #build message
            mymessage = (
                f"xxxx PLATFORM_ERROR xxxx\n"
                f" CMDR: {payload['cmdr_name']} -- Platform: {caseplatform} \n"
                f" System: {payload['system']} -- Hull: {payload['hull']} \n"
                f"xxxx NEWCASE xxxx\n"
            )
            #build short message
            myshortmessage = (
                f"New Case Available -- <@&744998165714829334\> \n"
                f" CMDR: {payload['cmdr_name']} Sys: {payload['system']} H: {payload['hull']} P: {caseplatform}\n"
            )

        #send messages
        #send message to back channel
        self.send(
          self.build_identifier("#case-notify"), myshortmessage,
        )

        #send long message to channels
        arfinchan = ["#Repair-Requests","#Seal-Bob"]
        for chan in arfinchan:
            self.send(
              self.build_identifier(chan), mymessage,
            )

        return "OK"

    @webhook
    def fishcase(self, payload):
        self.log.debug("The incoming fishcasepayload was :" + str(payload))

        #decode platform
        if payload.get('platform') == "1":
            caseplatform = "PC"
        elif payload.get('platform') == "2":
            caseplatform = "XB1"
        elif payload.get('platform') == "3":
            caseplatform = "PS4"
        else:
            caseplatform = "UNKNOWN"

        #decode type
        if payload.get('case_type') == "8":
            casetypeshort = "Lift"
        elif payload.get('case_type') == "9":
            casetypeshort = "Golf"
        elif payload.get('case_type') == "10":
            casetypeshort = "Puck"
        elif payload.get('case_type') == "11":
            casetypeshort = "Pick"
        else:
            casetypeshort = "UNKNOWN"

        if payload.get('platform') == "1":
            #build message
            mymessage = (
                f"xxxx PCKFCASE xxxx\n"
                f" CMDR: {payload['cmdr_name']} -- Platform: {caseplatform} \n"
                f" System: {payload['system']} -- Planet: {payload['planet']} \n"
                f" Coordinates: {payload['curr_cord']} \n"
                f" Type: {casetypeshort}\n"
                f"xxxx NEWCASE xxxx\n"
            )
            #build short message
            myshortmessage = (
                f"New PC Kingfisher Case Available -- <@&744998165714829334\> \n"
                f" CMDR: {payload['cmdr_name']} Sys: {payload['system']} Planet: {payload['planet']} P: {caseplatform} Cord: {payload['curr_cord']} Type: {casetypeshort}\n"
            )

        elif payload.get('platform') == "2":
            #build message
            mymessage = (
                f"xxxx XBKFCASE xxxx\n"
                f" CMDR: {payload['cmdr_name']} -- Platform: {caseplatform} \n"
                f" System: {payload['system']} -- Planet: {payload['planet']} \n"
                f" Coordinates: {payload['curr_cord']} \n"
                f" Type: {casetypeshort}\n"
                f"xxxx NEWCASE xxxx\n"
            )
            #build short message
            myshortmessage = (
                f"New XB1 Kingfisher Case Available -- <@&744998165714829334\> \n"
                f" CMDR: {payload['cmdr_name']} Sys: {payload['system']} Planet: {payload['planet']} P: {caseplatform} Cord: {payload['curr_cord']} Type: {casetypeshort}\n"
            )

        elif payload.get('platform') == "3":
            #build message
            mymessage = (
                f"xxxx PSKFCASE xxxx\n"
                f" CMDR: {payload['cmdr_name']} -- Platform: {caseplatform} \n"
                f" System: {payload['system']} -- Planet: {payload['planet']} \n"
                f" Coordinates: {payload['curr_cord']} \n"
                f" Type: {casetypeshort}\n"
                f"xxxx NEWCASE xxxx\n"
            )
            #build short message
            myshortmessage = (
                f"New PS4 Kingfisher Case Available -- <@&744998165714829334\> \n"
                f" CMDR: {payload['cmdr_name']} Sys: {payload['system']} Planet: {payload['planet']} P: {caseplatform} Cord: {payload['curr_cord']} Type: {casetypeshort}\n"
            )

        else:
            #build message
            mymessage = (
                f"xxxx PLATFORM_ERROR xxxx\n"
                f" CMDR: {payload['cmdr_name']} -- Platform: {caseplatform} \n"
                f" System: {payload['system']} -- Planet: {payload['planet']} \n"
                f" Coordinates: {payload['curr_cord']} \n"
                f" Type: {casetypeshort}\n"
                f"xxxx NEWCASE xxxx\n"
            )
            #build short message
            myshortmessage = (
                f"New Kingfisher Case Available -- <@&744998165714829334\> \n"
                f" CMDR: {payload['cmdr_name']} Sys: {payload['system']} Planet: {payload['planet']} P: {caseplatform} Cord: {payload['curr_cord']} Type: {casetypeshort}\n"
            )

        #send messages
        #send message to back channel
        self.send(
          self.build_identifier("#case-notify"), myshortmessage,
        )

        #send long message to channels
        arfinchan = ["#Repair-Requests","#Seal-Bob"]

        for chan in arfinchan:
            self.send(
              self.build_identifier(chan), mymessage,
            )

        return "OK"


    @botcmd
    def manualcase(self, msg, args):
        """Manually create case notification"""
        if args == "":
            #build message
            mymessage = (
                f"xxxx MANUAL_CASE xxxx\n"
                f" New Case in Repair Requests - Seals Report! \n"
                f"xxxx NEWCASE xxxx\n"
            )

            #build short message
            myshortmessage = (
                f"New Case Available -- <@&744998165714829334\> \n"
                f"New Case in Repair Requests - Seals Report! \n"
            )
        else:
            #build message
            mymessage = (
                f"xxxx MANUAL_CASE xxxx\n"
                f" {args} \n"
                f"xxxx NEWCASE xxxx\n"
            )

            #build short message
            myshortmessage = (
                f"New Case Available -- <@&744998165714829334\> \n"
                f" {args} \n"
            )

        #send messages
        #send message to back channel
        self.send(
          self.build_identifier("#case-notify"), myshortmessage,
        )

        #send long message to channels
        arfinchan = ["#Repair-Requests","#Seal-Bob"]
        for chan in arfinchan:
            self.send(
              self.build_identifier(chan), mymessage,
            )

        return


    @botcmd
    def mancase(self, msg, args):
        """Manually create case notification"""
        if args == "":
            #build message
            mymessage = (
                f"xxxx MANUAL_CASE xxxx\n"
                f" New Case in Repair Requests - Seals Report! \n"
                f"xxxx NEWCASE xxxx\n"
            )

            #build short message
            myshortmessage = (
                f"New Case Available -- <@&744998165714829334\> \n"
                f"New Case in Repair Requests - Seals Report! \n"
            )
        else:
            #build message
            mymessage = (
                f"xxxx MANUAL_CASE xxxx\n"
                f" {args} \n"
                f"xxxx NEWCASE xxxx\n"
            )

            #build short message
            myshortmessage = (
                f"New Case Available -- <@&744998165714829334\> \n"
                f" {args} \n"
            )
        #send messages
        #send message to back channel
        self.send(
          self.build_identifier("#case-notify"), myshortmessage,
        )

        #send long message to channels
        arfinchan = ["#Repair-Requests","#Seal-Bob"]
        for chan in arfinchan:
            self.send(
              self.build_identifier(chan), mymessage,
            )

        return

    @botcmd
    def manualfish(self, msg, args):
        """Manually create case notification"""
        if args == "":
            #build message
            mymessage = (
                f"xxxx MANUAL_CASE xxxx\n"
                f" New Case in Repair Requests - Fishers Report! \n"
                f"xxxx NEWKFCASE xxxx\n"
            )

            #build short message
            myshortmessage = (
                f"New Kingfisher Case Available -- <@&744998165714829334\> \n"
                f" New Case in Repair Requests - Fishers Report! \n"
            )
        else:
            #build message
            mymessage = (
                f"xxxx MANUAL_CASE xxxx\n"
                f" {args} \n"
                f"xxxx NEWKFCASE xxxx\n"
            )

            #build short message
            myshortmessage = (
                f"New Kingfisher Case Available -- <@&744998165714829334\> \n"
                f" {args} \n"
            )

        #send messages
        #send message to back channel
        self.send(
          self.build_identifier("#case-notify"), myshortmessage,
        )

        #send long message to channels
        arfinchan = ["#Repair-Requests","#Seal-Bob"]
        for chan in arfinchan:
            self.send(
              self.build_identifier(chan), mymessage,
            )

        return

    @botcmd
    def manfish(self, msg, args):
        """Manually create case notification"""
        if args == "":
            #build message
            mymessage = (
                f"xxxx MANUAL_CASE xxxx\n"
                f" New Case in Repair Requests - Fishers Report! \n"
                f"xxxx NEWKFCASE xxxx\n"
            )

            #build short message
            myshortmessage = (
                f"New Kingfisher Case Available -- <@&744998165714829334\> \n"
                f" New Case in Repair Requests - Fishers Report! \n"
            )
        else:
            #build message
            mymessage = (
                f"xxxx MANUAL_CASE xxxx\n"
                f" {args} \n"
                f"xxxx NEWKFCASE xxxx\n"
            )

            #build short message
            myshortmessage = (
                f"New Kingfisher Case Available -- <@&744998165714829334\> \n"
                f" {args} \n"
            )

        #send messages
        #send message to back channel
        self.send(
          self.build_identifier("#case-notify"), myshortmessage,
        )

        #send long message to channels
        arfinchan = ["#Repair-Requests","#Seal-Bob"]
        for chan in arfinchan:
            self.send(
              self.build_identifier(chan), mymessage,
            )

        return

    @botcmd
    def go(self, msg, args):
        """Indicate which Seals are assigned to the case."""
        if args == "":
            return "Mentioned Seals, You're up. All others, stand down."
        else:
            return args + ", You're up. All others, stand down."

    @botcmd
    def beacon(self, msg, args):
        """How to turn on your beacon."""
        if args == "":
            return "Client: To turn on your beacon, Go to the right-hand menu, and select to the 'Ship' tab. (1/2) \n Under Functions, select 'Beacon', and then select 'Wing'. (2/2)"
        else:
            return args + ": To turn on your beacon, Go to the right-hand menu, and select to the 'Ship' tab. (1/2) \n Under Functions, select 'Beacon', and then select 'Wing'. (2/2)"

    @botcmd
    def cbinfo(self, msg, args):
        """Manually Fired Code Black Cases"""
        if args == "":
            return "## DO NOT LOG IN and provide the following info if you know it: (1/2) \n CMDR Name, Current System, Hull %, O2 Time Left, Platform, and if you have any life support synthesis. (2/2)"
        else:
            return "## " + args + ": DO NOT LOG IN and provide the following info if you know it: (1/2) \n CMDR Name, Current System, Hull %, O2 Time Left, Platform, and if you have any life support synthesis. (2/2)"

    @botcmd
    def cbmining(self, msg, args):
        """How to mine without a HUD during Code Blacks."""
        if args == "":
            return "Client: You can find the location of objects using the radar module. Select the item you want on the left hand panel, then go towards it using your radar. (1/2) \n Open your cargo scoop, and then go towards the item. Move your ship above the item slightly, then move forward with a speed of less then 40. (2/2)"
        else:
            return args + ": You can find the location of objects using the radar module. Select the item you want on the left hand panel, then go towards it using your radar. (1/2) \n Open your cargo scoop, and then go towards the item. Move your ship above the item slightly, then move forward with a speed of less then 40. (2/2)"

    @botcmd
    def clientinfo(self, msg, args):
        """Manually Fired Seal Cases"""
        if args == "":
            return "## Greetings, CMDR. Please provide the following information: (1/2) \n CMDR Name, Current System, Hull %, and Platform. If your canopy is breached, LOG OUT IMMEDIATELY. (2/2)"
        else:
            return "## " + args + ": Greetings, CMDR. Please provide the following information: (1/2) \n CMDR Name, Current System, Hull %, and Platform. If your canopy is breached, LOG OUT IMMEDIATELY. (2/2)"

    @botcmd
    def drill(self, msg, args):
        """Drill Start Commands"""
        if args == "":
            return "## DRILL DRILL DRILL (1/3) \n Greetings, CMDR. Please provide the following information: (2/3) \n CMDR Name, Current System, Hull %, and Platform. If your canopy is breached, LOG OUT IMMEDIATELY. (3/3)"
        else:
            return "## DRILL DRILL DRILL (1/3) \n "+ args + ": Greetings, CMDR. Please provide the following information: (2/3) \n CMDR Name, Current System, Hull %, and Platform. If your canopy is breached, LOG OUT IMMEDIATELY. (3/3)"

    @botcmd
    def escapeneutron(self, msg, args):
        """How to escape Neutron Stars."""
        if args == "":
            return "## Here's a Guide to Escaping a Neutron Cone (1/2) \n Text: https://hullse.al/enText --- Video: https://hullse.al/enVid (2/2)"
        else:
            return "## " + args + ": Guide to Escaping a Neutron Cone (1/2) \n Text: https://hullse.al/enText --- Video: https://hullse.al/enVid (2/2)"

    @botcmd
    def paperwork(self, msg, args):
        """Tell Seals to do their paperwork."""
        if args == "":
            return "Seal: Don't forget to do your paperwork! http://hullse.al/ppwk"
        else:
            return args + ": Don't forget to do your paperwork! http://hullse.al/ppwk"

    @botcmd
    def pw(self, msg, args):
        """Tell Seals to do their paperwork."""
        if args == "":
            return "Seal: Don't forget to do your paperwork! http://hullse.al/ppwk"
        else:
            return args + ": Don't forget to do your paperwork! http://hullse.al/ppwk"

    @botcmd
    def clear(self, msg, args):
        """Tell Seals to do their paperwork."""
        if args == "":
            return "Seal: Don't forget to do your paperwork! http://hullse.al/ppwk"
        else:
            return args + ": Don't forget to do your paperwork! http://hullse.al/ppwk"

    @botcmd
    def pcfr(self, msg, args):
        """Instruct clients on how to add a Friend on PC"""
        if args == "":
            return "Client: Please tap ESC to go to the PAUSE menu, click SOCIAL, enter the CMDR name in the box, and click ADD FRIEND on their name."
        else:
            return args + ": Please tap ESC to go to the PAUSE menu, click SOCIAL, enter the CMDR name in the box, and click ADD FRIEND on their name."

    @botcmd
    def psfr(self, msg, args):
        """Instruct clients on how to add a Friend on PS"""
        if args == "":
            return "Client: Please tap your PlayStation button, then open the Friends screen from Functions, search for your Seal's name, and add them as your friend."
        else:
            return args + ": Please tap your PlayStation button, then open the Friends screen from Functions, search for your Seal's name, and add them as your friend."

    @botcmd
    def xbfr(self, msg, args):
        """Instruct clients on how to add a Friend on XB"""
        if args =="":
            return "Client: Please tap your XBOX button, then go to the Social tab and search for your Seal's name. Be sure to add the Seal(s) as your friend."
        else:
            return args + ": Please tap your XBOX button, then go to the Social tab and search for your Seal's name. Be sure to add the Seal(s) as your friend."

    @botcmd
    def wing(self, msg, args):
        """How to add a Seal to your wing."""
        if args =="":
            return "Client: Please open your Comms Panel (top left option), and select the third tab. Then, select the Seal(s) you want, and select Invite to Wing."
        else:
            return args + ": Please open your Comms Panel (top left option), and select the third tab. Then, select the Seal(s) you want, and select Invite to Wing."

    @botcmd
    def stuck(self, msg, args):
        """Manually Fired Fisher Cases"""
        if args == "":
            return "## Greetings, CMDR. Please provide the following information: (1/2) \n CMDR Name, Current System, Current Planet/Moon, Your Current Coordinates, your Platform, and what services you require! (2/2)"
        else:
            return "## " + args + ": Greetings, CMDR. Please provide the following information: (1/2) \n CMDR Name, Current System, Current Planet/Moon, Your Current Coordinates, your Platform, and what services you require! (2/2)"

    @botcmd
    def prep(self, msg, args):
        """Get Clients ready for their Seal"""
        if args == "":
            return "Client: If you are landed, take off and move to at least 5Ls from any body. (1/2) \n Please drop from Supercruise, set your throttle to zero, and deploy your landing gear while you wait. (2/2)"
        else:
            return args + ": If you are landed, take off and move to at least 5Ls from any body. (1/2) \n Please drop from Supercruise, set your throttle to zero, and deploy your landing gear while you wait. (2/2)"

    @botcmd
    def verify(self, msg, args):
        """Verify information from Clients"""
        if args == "":
            return "## We just need to check some things, CMDR. Please confirm for us: (1/4) \n Your Current Game Mode, CMDR name, and current system. (2/4) \n **Please also provide a screenshot of your cockpit view, with system and date visible. We encourage programs like imgur or Discord for image hosting. (3/4)** \n Seals, please hold position while we check. (4/4)"
        else:
            return "## " + args + ": We just need to check some things. Please confirm for us: (1/4) \n Your Current Game Mode, CMDR name, and current system. (2/4) \n **Please also provide a screenshot of your cockpit view, with system and date visible. We encourage programs like imgur or Discord for image hosting. (3/4)** \n Seals, please hold position while we check. (4/4)"

    @botcmd
    def chatter(self, msg, args):
        """Contain your Chatter, Seals!"""
        return "** - - - - - - - - - - - - - - - - ** \n **Seals, please keep chatter to #seal-bob. Thank you.**"

    @botcmd
    def join(self, msg, args):
        """Information on how to join."""
        if args == "":
            return args + ": Greetings, CMDR! Welcome to the Hull Seals. Here's some info to get you started on joining us: https://hullse.al/How2Join"
        else:
            return "Greetings, CMDR! Welcome to the Hull Seals. Here's some info to get you started on joining us: https://hullse.al/How2Join"

    @botcmd
    def bacon(self, msg, args):
        """Light up that bacon!"""
        return "The bacon is lit! -- https://youtu.be/Wd2qRSzCj84"

    @botcmd
    def fuel(self, msg, args):
        """Redirect clients to the Rats"""
        if args == "":
            return args + ": For fuel emergencies, your best bet is the Fuel Rats: https://fuelrats.com/i-need-fuel"
        else:
            return args + "For fuel emergencies, your best bet is the Fuel Rats: https://fuelrats.com/i-need-fuel"

    @botcmd
    def cmdlist(self, msg, args):
        """List of current Commands"""
        if args == "":
            return args + ": Common HalpyBOT Commands: http://hullse.al/cmdlist"
        else:
            return "Common HalpyBOT Commands: http://hullse.al/cmdlist"

    @botcmd
    def welcome(self, msg, args):
        """Welcome a new client"""
        if args == "":
            return args + ": Welcome to the Hull Seals! We've got your case details and we'll be with you shortly. Just take a breath, stay calm, and follow all instructions given to you by Dispatchers."
        else:
            return "Welcome to the Hull Seals! We've got your case details and we'll be with you shortly. Just take a breath, stay calm, and follow all instructions given to you by Dispatchers."

    @botcmd
    def tos(self, msg, args):
        """Link to our TOS"""
        if args == "":
            return args + ": Our Terms of Service and Important Info: https://hullse.al/ImportantInfo"
        else:
            return "Our Terms of Service and Important Info: https://hullse.al/ImportantInfo"

    @botcmd
    def highg(self, msg, args):
        """How to take off from High G Worlds"""
        if args == "":
            return "Client: Remain landed until instructed by your dispatcher. When instructed, use your VERTICAL THRUSTERS ONLY to rise up to at least 6km above the surface. (1/2) \n Keep your landing gear deployed until you are ready to jump to supercruise, and keep your ship aligned horizontally with the planet at all times.  (2/2)"
        else:
            return args + ": Remain landed until instructed by your dispatcher. When instructed, use your VERTICAL THRUSTERS ONLY to rise up to at least 6km above the surface. (1/2) \n Keep your landing gear deployed until you are ready to jump to supercruise, and keep your ship aligned horizontally with the planet at all times.  (2/2)"

    @botcmd
    def synth(self, msg, args):
        """How to Synthesize a life support refill."""
        if args == "":
            return "Client: To synth a new life support refill, Go to the right-hand menu, and select to the 'Inventory' tab. (1/3) \n Go down to 'Synthesis' (the second to last option), choose 'Life Support', and then select 'Resupply Life Support'. (2/3) \n This requires you have 2 iron and 1 nickel in your materials reserve. (3/3)"
        else:
            return args + ": To synth a new life support refill, Go to the right-hand menu, and select to the 'Inventory' tab. (1/3) \n Go down to 'Synthesis' (the second to last option), choose 'Life Support', and then select 'Resupply Life Support'. (2/3) \n This requires you have 2 iron and 1 nickel in your materials reserve. (3/3)"
