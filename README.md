# err-halpybot-newcase-plugin

This is the plugin for HalpyBot to respond/handle cases in #repair-requests, #Seal-bob, #case-notify


To make changes:

**On gitlab**

    edit plugin files

    commit changes to master branch

**On IRC Prv message Halpybot**

    !repos update all

    !restart


To enable webserver for web hook:

    !plugin config Webserver {'HOST': '0.0.0.0','PORT': 3141,'SSL': {'certificate': '','enabled': False,'host': '0.0.0.0','key': '','port': 3142}}

